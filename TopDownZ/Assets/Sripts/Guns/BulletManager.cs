﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject _prefBullet;
    private List<GameObject> _activeBuletList, _inactiveBulletList;

    private static BulletManager instance = null;

    public static BulletManager Instance {
        get {
            if (instance == null)
                instance = GameObject.Find("SpawnManager").transform.gameObject.AddComponent<BulletManager>();
            return instance;
        }
    }

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            if (instance == this) {
                Destroy(gameObject);
            }
        }
    }

    private void Start() {
        _activeBuletList = new List<GameObject>();
        _inactiveBulletList = new List<GameObject>();
    }

    public GameObject GetBullet(Vector3 position, Quaternion rotation) {//выдаем имеющуюся пулю или создаем новую(пулл объектов)
        if (_inactiveBulletList.Count < 1) {
            return CreateNewBullet(position, rotation);
        } else {
            GameObject bullet = _inactiveBulletList[0];
            _inactiveBulletList.Remove(bullet);
            _activeBuletList.Add(bullet);
            bullet.SetActive(true);
            bullet.transform.position = position;
            bullet.transform.rotation = rotation;
            return bullet;
        }
    }

    public void DeactivateBullet(GameObject bullet) {//Выключаем пулю
        _inactiveBulletList.Add(bullet);
        _activeBuletList.Remove(bullet);
        bullet.SetActive(false);
    }

    private GameObject CreateNewBullet(Vector3 position, Quaternion rotation) {
        GameObject bullet = GameObject.Instantiate(_prefBullet, position, rotation);
        bullet.transform.parent = transform;
        _inactiveBulletList.Add(bullet);
        return bullet;
    }
}

