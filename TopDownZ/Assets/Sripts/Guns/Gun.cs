﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun : MonoBehaviour
{
    [SerializeField, Range(.01f, 10f)]
    protected float _coolDown = 1f;
    [SerializeField, Range(0f, 5f)]
    protected float _range = 0.5f;
    protected GameObject _trigger_Right;
    protected bool _ready = true;
    protected GameObject _bulletPosition;

    protected BulletManager _bm;

    private bool _shoot = false;

    private void Awake() {
        _trigger_Right = GameObject.Find("Trigger_Right");
        _bulletPosition = GameObject.Find("BulletPoint");
    }

    protected virtual void Start() {
        _bm = BulletManager.Instance;
    }

    protected virtual void Shoot() {
        var rotationY = Quaternion.AngleAxis(Random.Range(-_range, _range), transform.up);
        var rotationX = Quaternion.AngleAxis(Random.Range(-_range, _range), transform.right);
        _bm.GetBullet(_bulletPosition.transform.position, transform.rotation * rotationX * rotationY);
    }

    protected void Update() {
        if (Input.GetMouseButtonDown(0)) {
            _shoot = true;
        }
        if (Input.GetMouseButtonUp(0)) {
            _shoot = false;
        }
        if (_shoot) {
            if (_ready) {
                Shoot();
                _ready = false;
                StartCoroutine(CollDown());
            }
        }
    }

    private IEnumerator CollDown() {
        yield return new WaitForSeconds(_coolDown);
        _ready = true;
    }
}
