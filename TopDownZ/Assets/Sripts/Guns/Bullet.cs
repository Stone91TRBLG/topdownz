﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField, Range(1f, 100f)]
    private float _speed = 10f;
    [SerializeField, Range(1f, 100f)]
    private float _damage = 1f;
    private Vector3 _lastPosition;
    protected BulletManager _bm;

    private void Start()
    {
        _lastPosition = transform.position;
        _bm = BulletManager.Instance;
    }

    private void Update()
    {
        transform.Translate(_speed * Time.deltaTime * Vector3.up);
        RaycastHit hit;
        if(Physics.Linecast(_lastPosition, transform.position, out hit)) {
            if(hit.transform.tag == "Enemy") {
                hit.transform.gameObject.GetComponent<Zombie>().SetDamage(_damage); 
            }
            _bm.DeactivateBullet(gameObject);
        }
        _lastPosition = transform.position;
    }
}
