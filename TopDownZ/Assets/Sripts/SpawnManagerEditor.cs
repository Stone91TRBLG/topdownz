﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

    [CustomEditor(typeof(SpawnManager))]
public class SpawnManagerEditor : Editor {

    float minVal = 40;
    int minLimit = 1;
    float maxVal = 60;
    int maxLimit = 100;

    public override void OnInspectorGUI() {
        SpawnManager sm = (SpawnManager)target;
        EditorGUILayout.MinMaxSlider(ref minVal, ref maxVal, minLimit, maxLimit);

        sm.MinZombie = (int)minVal;
        sm.MaxZombie = (int)maxVal;

        base.OnInspectorGUI();
    }
}
