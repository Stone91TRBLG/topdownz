﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField, Range(1f, 10f)]
    private float _speed = 5f;
    [SerializeField, Range(0.1f, 5f)]
    private float _maxSpeed = 1f;
    private GameObject _player;
    private GameObject _aim;//прицел
    private Vector3 _target;//Точка в которую должна перейти камера
    private Vector3 _dx; //текущее отколнение координат камеры от игрока в сторону _target

    
    void Start()
    {
        _player = GameObject.FindWithTag("Player");
        _aim = GameObject.Find("Aim");
        _dx = new Vector3(_player.transform.position.x, transform.position.y, _player.transform.position.z);
    }
 
    void Update()
    {
        _target = new Vector3((_aim.transform.position.x - _player.transform.position.x) / 3,
            transform.position.y, (_aim.transform.position.z - _player.transform.position.z) / 3); //получаем точку в которую нужно перевести камеру

        Vector3 vect = new Vector3(_player.transform.position.x, transform.position.y, _player.transform.position.z);//точка координат игрока но с высотой камеры

        float speed = _speed * Time.deltaTime * (Vector3.Distance(_target, transform.position));
        if (speed > _maxSpeed) speed = _maxSpeed;
        _dx = Vector3.MoveTowards(_dx, _target, speed); //_speed * Time.deltaTime); //ДОБАВИТЬ К СКОРОСТИ ДИСТАНЦИЮ ДО ТОЧКИ

        transform.position = new Vector3(_player.transform.position.x + _dx.x, transform.position.y, _player.transform.position.z + _dx.z);
    }
}
