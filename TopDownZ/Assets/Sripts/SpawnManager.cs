﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    private static SpawnManager instance = null;
    [SerializeField]
    private int _minZombie = 0;
    public int MinZombie {
        get {
            return _minZombie;
        }

        set {
            _minZombie = value;
        }
    }

    [SerializeField]
    private int _maxZombie = 0;
    public int MaxZombie {
        get {
            return _maxZombie;
        }

        set {
            _maxZombie = value;
        }
    }

    [SerializeField]
    protected GameObject _prefZombie;
    [SerializeField]
    private List<GameObject> _activeZombieList, _inactiveZombieList;
    [SerializeField]
    private List<GameObject> _spawnPoints;

    public static SpawnManager Instance {
        get {
            if (instance == null)
                instance = GameObject.Find("SpawnManager").transform.gameObject.AddComponent<SpawnManager>();
            return instance;
        }
    }

    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            if (instance == this) {
                Destroy(gameObject);
            }
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Backspace)) {
            Spawn();
        }
    }

    public void Spawn() {
        Spawn(1);
    }

    public void Spawn(int wave) {
        int count = Random.Range(_minZombie, _maxZombie);
        count += (int)(0.3f * count * (wave - 1)); //увеличиваем колличество зомби на 30% с каждой волной
        for(int i = 0; i < count; i++) {
            if (_inactiveZombieList.Count < 1) {
                CreateNewZombie(GenerateNewZombiePosition());
            } else {
                GameObject zombie = _inactiveZombieList[0];
                _inactiveZombieList.Remove(zombie);
                _activeZombieList.Add(zombie);
                zombie.GetComponent<Zombie>().Heal();
                zombie.SetActive(true);
                zombie.transform.position = GenerateNewZombiePosition();
            }
        }
    }

    private GameObject CreateNewZombie(Vector3 position) {
        GameObject zombie = GameObject.Instantiate(_prefZombie, GenerateNewZombiePosition(), _prefZombie.transform.rotation);
        zombie.transform.parent = transform;
        zombie.SetActive(true);
        _activeZombieList.Add(zombie);
        return zombie;
    }

    private Vector3 GenerateNewZombiePosition() {
        int spawn = Random.Range(0, _spawnPoints.Count);
        return _spawnPoints[spawn].transform.position;
    }

    public void DeactivateZombie(GameObject zombie) {//Выключаем зомби
        _inactiveZombieList.Add(zombie);
        _activeZombieList.Remove(zombie);
        zombie.SetActive(false);
    }

    public int GetCountZombie() {//возвращает количество активных зомби на сцене
        return _activeZombieList.Count;
    }
}
