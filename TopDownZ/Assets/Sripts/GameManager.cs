﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField, Range(1, 10)]
    private int _countWave = 3;
    private int _currentWave = 0;
    private SpawnManager _sm;
    private GameObject _endText;
    private bool _isFinished = false; //закончена ли игра
    private static GameManager instance = null;
    public static GameManager Instance {
        get {
            if (instance == null)
                instance = GameObject.Find("GameManager").transform.gameObject.AddComponent<GameManager>();
            return instance;
        }
    }

    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            if (instance == this) {
                Destroy(gameObject);
            }
        }
    }

    private void Start()
    {
        _sm = SpawnManager.Instance;
        StartCoroutine(Spawn());
        _endText = GameObject.Find("EndText");
        _endText.SetActive(false);
    }

    private void Update() {
        if (_isFinished) {
            if (Input.GetKeyDown(KeyCode.F5)) {
                SceneManager.LoadScene(0);
            }
        }
    }

    private IEnumerator Spawn() {
        while (true) {
            yield return new WaitForSeconds(1f);
            while (_currentWave < _countWave) {
                yield return new WaitForSeconds(1f);
                if (_sm.GetCountZombie() == 0) {
                    _sm.Spawn(_currentWave);
                    _currentWave++;
                }
            }
            if (_currentWave == _countWave && _sm.GetCountZombie() == 0) {
                EndGame(true);
            }
        }
    } 

    public void EndGame(bool WonOrLost) {
        _endText.SetActive(true);
        _isFinished = true;
    }
}
