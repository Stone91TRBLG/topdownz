﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    [SerializeField]
    private float _sensitivity = 2f;

    private float _xDir = 0;
    private float _yDir = 0;
    private float _distance = 10f;

    private GameObject _player;

    private void Start() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _player = GameObject.FindWithTag("Player");

    }

    void Update() {
        _xDir = Input.GetAxis("Mouse X") * _sensitivity;
        _yDir = Input.GetAxis("Mouse Y") * _sensitivity;
        Vector3 vect = new Vector3(_xDir * Time.deltaTime * _sensitivity, 0f, _yDir * Time.deltaTime * _sensitivity);
        transform.position += vect;

        #region проверяем прицел по границам камеры
        Vector3 clampedPos = transform.position;
        clampedPos.x = Mathf.Clamp(clampedPos.x,  _player.transform.position.x - _distance, _player.transform.position.x + _distance);
        clampedPos.z = Mathf.Clamp(clampedPos.z, _player.transform.position.z - _distance, _player.transform.position.z + _distance);
        transform.position = clampedPos;
        #endregion
    }
}
