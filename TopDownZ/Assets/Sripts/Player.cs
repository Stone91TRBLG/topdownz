﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    [SerializeField, Range(10f, 200f)]
    private float _maxHP = 100f, _curentHP = 100f;
    [SerializeField, Range(2f, 15f)]
    private float _speed = 4f;
    [SerializeField, Range(5f, 20f)]
    private float _speedRotation = 10f;
    private float _horizontal = 0f;
    private float _vertical = 0f;
    private Vector3 _moveDir = Vector3.zero;
    private CharacterController _characterController;
    private GameObject _aim;//прицел

    void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _aim = GameObject.Find("Aim");
    }

    void FixedUpdate()
    {
        Move();
        UpdateRotation();
    }

    public void SetDamage(float points) {//метод вызываемы для получения урона
        _curentHP -= points;
        if(_curentHP <= 0f) {
            Dead();
        }
    }

    private void Move() {
        _horizontal = Input.GetAxis("Horizontal");
        _vertical = Input.GetAxis("Vertical");

        Vector2 input = new Vector2(_horizontal, _vertical);

        if (input.sqrMagnitude > 1) {
            input.Normalize();
        }

        Vector3 desiredMove = new Vector3(input.x, 0, input.y);
        _moveDir.x = desiredMove.x * _speed;
        _moveDir.z = desiredMove.z * _speed;
        _moveDir += Physics.gravity * Time.fixedDeltaTime;
        _characterController.Move(_moveDir * Time.fixedDeltaTime);
    }

    private void UpdateRotation() {
        var direction = (_aim.transform.position - transform.position).normalized;
        direction.y = 0f;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), _speedRotation);
    }

    private void Dead() {
        gameObject.SetActive(false);
        GameManager.Instance.EndGame(false);
    }
}
