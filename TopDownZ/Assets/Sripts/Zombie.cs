﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    [SerializeField, Range(0.2f, 3f)]
    private float _distance = 1f;
    [SerializeField, Range(1, 100)]
    private float _damage = 1;
    [SerializeField, Range(10f, 200f)]
    private float _maxHP = 100f, _curentHP = 100f;
    private GameObject _player;
    private NavMeshAgent _agent;
    private Player _playerScript;
    private Animator _anim;
    private SpawnManager _sm;

    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _player = GameObject.FindWithTag("Player");
        _playerScript = _player.GetComponent<Player>();
        _anim = GetComponent<Animator>();
        _sm = SpawnManager.Instance;
    }

    void Update() {
        var animStateInfo = _anim.GetCurrentAnimatorStateInfo(0);
        if (animStateInfo.IsName("walk")) {
            if (Vector3.Distance(gameObject.transform.position, _player.transform.position) > _distance) {
                MoveTo(_player.transform.position);
            } else {
                Attack();
            }
        }
    }

    public void SetDamage(float points) {//метод вызываемы для получения урона
        _curentHP -= points;
        if (_curentHP <= 0f) {
            Dead();
        }
    }

    public void MoveTo(Vector3 point) {//идти  в точку
        _agent.SetDestination(point);
        _agent.isStopped = false;
        _anim.SetTrigger("walk");
    }

    public void Attack() {
        _playerScript.SetDamage(_damage);
        _anim.SetTrigger("attack");
        _agent.isStopped = true;
    }

    private void Dead() {
        _sm.DeactivateZombie(gameObject);
    }

    public void Heal() {
        _curentHP = _maxHP;
    }
}
